<?php

class ThreadsController extends \BaseController {

	/**
	 * Display a listing of threads
	 *
	 * @return Response
	 */
	public function index()
	{
		$threads = Thread::all();

		return View::make('threads.index', compact('threads'));
	}

	/**
	 * Show the form for creating a new thread
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('threads.create');
	}

	/**
	 * Store a newly created thread in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Thread::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Thread::create($data);

		return Redirect::route('threads.index');
	}

	/**
	 * Display the specified thread.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$thread = Thread::findOrFail($id);

		return View::make('threads.show', compact('thread'));
	}

	/**
	 * Show the form for editing the specified thread.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$thread = Thread::find($id);

		return View::make('threads.edit', compact('thread'));
	}

	/**
	 * Update the specified thread in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$thread = Thread::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Thread::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$thread->update($data);

		return Redirect::route('threads.index');
	}

	/**
	 * Remove the specified thread from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Thread::destroy($id);

		return Redirect::route('threads.index');
	}

}
